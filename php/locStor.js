function onChange(inpId){
 	let el=document.getElementById(inpId);
	let key=el.options[el.selectedIndex].text;
	if(inpId==2 && key!="expired")
		return;
	var item=localStorage.getItem(key);
	if(item ===null){
		alert("Нема збережених данів щодо такого запиту");
		return;
	}
	document.getElementById('table').innerHTML=item;
}
function onLoad(){
	var table=document.getElementById('table');
	if(table ===null) return;
	var key=document.getElementById('key').value;
	localStorage.setItem(key, table.innerHTML);
}
